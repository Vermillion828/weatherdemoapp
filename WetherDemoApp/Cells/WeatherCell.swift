//
//  WeatherCell.swift
//  WetherDemoApp
//
//  Created by Vermillion on 7/17/18.
//  Copyright © 2018 com.agilie. All rights reserved.
//

import UIKit

class WeatherCell: UICollectionViewCell {
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var forecastText: UILabel!
    
}
