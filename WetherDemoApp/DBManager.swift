//
//  DBManager.swift
//  WetherDemoApp
//
//  Created by Vermillion on 7/16/18.
//  Copyright © 2018 com.agilie. All rights reserved.
//

import Foundation
import RealmSwift

class DBManager {
    
    func saveToDB(forecast: Forecast) {
        var realm:Realm
        try! realm = Realm()
        if (!realm.isInWriteTransaction) {
            do {
                try realm.write {
                    realm.add(forecast, update: true)
                }
            } catch {
                
            }
        }
    }
    
    func clearOldData() {
        var realm:Realm
        try! realm = Realm()
        if !realm.isInWriteTransaction {
            do {
                try realm.write {
                    realm.deleteAll()
                }
            } catch { }
        }
    }
    
    func getForecast() -> [Forecast] {
        var realm:Realm
        try! realm = Realm()
        let result = realm.objects(Forecast.self)
        var forecastArr:[Forecast] = []
        result.forEach { (item) in
            forecastArr.append(item)
        }
        return forecastArr
    }
}
