//
//  Forecast.swift
//  WetherDemoApp
//
//  Created by Vermillion on 7/16/18.
//  Copyright © 2018 com.agilie. All rights reserved.
//

import Foundation
import RealmSwift

class Forecast : Object {
    
    @objc dynamic var forecastDateTime: String? = "" //2012-07-24_00:00:00
    @objc dynamic var day: String? = "" //24
    @objc dynamic var time: String? = "" //00:00
    @objc dynamic var precipitation: String? = "" //"небольшой дождь"
    @objc dynamic var precipitationIconType = 0 //0-13
    @objc dynamic var temperature_low = 0.0
    @objc dynamic var temperature_high = 0.0
    @objc dynamic var cloud: String? = "" //"облачно"
    
    override class func primaryKey() -> String? {
        return "forecastDateTime"
    }
}
