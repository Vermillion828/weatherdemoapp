//
//  NetworkManager.swift
//  WetherDemoApp
//
//  Created by Vermillion on 7/16/18.
//  Copyright © 2018 com.agilie. All rights reserved.
//

import Foundation
import AFNetworking

class NetworkManager {
    
    func getWeather() {
        let manager = AFHTTPSessionManager()
        manager.requestSerializer.setValue("b9b3e3d337d48a25debfc42f36ba83dab19b7c89", forHTTPHeaderField: "Token")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.get("http://www.meteoprog.ua/ru/api/city/Dnipropetrovsk", parameters: nil, progress: nil, success: { (task, response) in
            //ok
            if let resObj = response {
                if let weather = resObj as? Dictionary<String, Any> {
                    DBManager().clearOldData()
                    Parser().parseJSON(dict: weather)
                    NotificationCenter.default.post(Notification(name: NSNotification.Name(rawValue: kRefreshNotificationKey)))
                }
            }
        }) { (task, error) in
            //fail
            NotificationCenter.default.post(Notification(name: NSNotification.Name(rawValue: kRefreshNotificationKey)))
        }
    }
}
