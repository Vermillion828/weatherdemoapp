//
//  Parser.swift
//  WetherDemoApp
//
//  Created by Vermillion on 7/16/18.
//  Copyright © 2018 com.agilie. All rights reserved.
//

import Foundation

class Parser {
    
    let timesArr = ["00:00:00", "06:00:00", "12:00:00", "18:00:00"]
    
    func parseJSON(dict:Dictionary<String, Any>) {
        if let data = dict["data"] as? Dictionary<String, Any> {
            if let weather = data["weather"] as? Dictionary<String, Any> {
                let dataKeys = Array(weather.keys)
                let sortedKeys = dataKeys.sorted { $0 < $1 }
                
                sortedKeys.forEach { (key) in
                    if let dict = weather[key] as? Dictionary<String, Any> {
                        if let forecast = dict["forecast"] as? Dictionary<String, Any> {
                            timesArr.forEach({ (timeKey) in
                                if let forecastForTime = forecast[timeKey] as? Dictionary<String, Any> {
                                    saveForecast(forecastForTime: forecastForTime, key: key, timeKey: timeKey)
                                }
                            })
                        }
                    }
                }
            }
        }
    }
    
    func saveForecast(forecastForTime:Dictionary<String, Any>, key: String, timeKey: String) {
        let forecast = Forecast()
        forecast.forecastDateTime = "\(key)_\(timeKey)"
        forecast.time = String(timeKey.prefix(5))
        if forecast.time == "00:00" {
            forecast.day = String(key.suffix(2))
        }
        
        if let precipitation = forecastForTime["precipitation"] as? Dictionary<String, Any> {
            if let name = precipitation["name"] as? String {
                forecast.precipitation = name
            }
            if let type = precipitation["type"] as? Int {
                forecast.precipitationIconType = type
            }
        }
        if let temperature = forecastForTime["temperature"] as? Dictionary<String, Any> {
            if let low = temperature["low"] as? Double {
                forecast.temperature_low = low
            }
            if let high = temperature["high"] as? Double {
                forecast.temperature_high = high
            }
        }
        if let cloud = forecastForTime["cloud"] as? Dictionary<String, Any> {
            if let name = cloud["name"] as? String {
                forecast.cloud = name
            }
        }
        DBManager().saveToDB(forecast: forecast)
    }
    
}
