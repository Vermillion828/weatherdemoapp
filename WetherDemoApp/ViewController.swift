//
//  ViewController.swift
//  WetherDemoApp
//
//  Created by Vermillion on 7/16/18.
//  Copyright © 2018 com.agilie. All rights reserved.
//

import UIKit
import Charts

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ChartViewDelegate {
    
    var forecastArr:[Forecast] = []
    var options: [Option]!
    
    lazy var formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 0
        formatter.negativeSuffix = "C°"
        formatter.positiveSuffix = "C°"
        
        return formatter
    }()
    
    @IBOutlet weak var weatherCollectionView: UICollectionView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet var chartView: BarChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        weatherCollectionView.register(UINib(nibName: "WeatherCell", bundle: Bundle.main), forCellWithReuseIdentifier: "cell")
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.refresh), name: Notification.Name(kRefreshNotificationKey), object: nil)
        setupChart()
    }

    @objc func refresh() {
        forecastArr = DBManager().getForecast()
        switch segmentControl.selectedSegmentIndex {
        case 0:
            forecastArr.removeSubrange(Range(NSRange(location: 4, length: (forecastArr.count - 4)))!)
            setChartData(count: 1)
            break
        case 1:
            forecastArr.removeSubrange(Range(NSRange(location: 12, length: (forecastArr.count - 12)))!)
            setChartData(count: 3)
            break
        default:
            setChartData(count: 7)
            break
        }
        weatherCollectionView.reloadData()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forecastArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: WeatherCell? = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? WeatherCell
        let forecast = forecastArr[indexPath.item]
        cell?.dayLabel.text = forecast.day
        cell?.timeLabel.text = forecast.time
        cell?.weatherIcon.image = UIImage(named: "\(forecast.precipitationIconType).png")
        let low = forecast.temperature_low > 0 ? "+\(forecast.temperature_low)" : "-\(forecast.temperature_low)"
        let high = forecast.temperature_high > 0 ? "+\(forecast.temperature_high)" : "-\(forecast.temperature_high)"
        cell?.temperatureLabel.text = "\(low)..\(high)"
        cell?.forecastText.text = "\(forecast.cloud!), \(forecast.precipitation!)"
        return cell!
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width / 4, height: 282)
    }
    
    // MARK: - UISegmentControl
    
    @IBAction func segmentControlValueDIdChanged(_ sender: Any) {
        refresh()
    }
    
    // MARK: - Chart
    
    func setupChart() {
        
        chartView.delegate = self
        chartView.chartDescription?.enabled = false
        chartView.maxVisibleCount = 50
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = false
        chartView.highlightFullBarEnabled = false
        let leftAxis = chartView.leftAxis
        leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: formatter)
        leftAxis.axisMinimum = 0
        chartView.rightAxis.enabled = false
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .top
        xAxis.labelFont = .systemFont(ofSize: 10)
        xAxis.granularity = 1
        xAxis.valueFormatter = DayAxisValueFormatter(chart: chartView)
    }
    
    func setChartData(count: Int) {
        chartView.data = nil
        let date = Date()
        let cal = Calendar.current
        if let day = cal.ordinality(of: .day, in: .year, for: date) {
            let yVals = ((day + 1)..<(day + 1) + count).map { (i) -> BarChartDataEntry in
                let to = (i - day) * 4
                let from  = to - 4
                var min = 0.0
                var max = 0.0
                for index in from ..< to {
                    let forecast = forecastArr[index]
                    if min == 0.0 || forecast.temperature_low < min {
                        min = forecast.temperature_low
                    }
                    if max == 0.0 || forecast.temperature_high > max {
                        max = forecast.temperature_high - min
                    }
                }
                return BarChartDataEntry(x: Double(i), yValues: [min, max])
            }
            let set = BarChartDataSet(values: yVals, label: "C°")
            set.drawIconsEnabled = false
            set.colors = [ChartColorTemplates.material()[0], ChartColorTemplates.material()[1]]
            set.stackLabels = ["Мин", "Макс"]
            let data = BarChartData(dataSet: set)
            data.setDrawValues(false)
            chartView.fitBars = true
            chartView.data = data
        }
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        
        let date = Date()
        let cal = Calendar.current
        if let day = cal.ordinality(of: .day, in: .year, for: date) {
            let index = Int(entry.x) - (day + 1)
            weatherCollectionView.scrollToItem(at: IndexPath(item: (index * 4), section: 0), at: UICollectionViewScrollPosition.left, animated: true)
        }
    }
}
